import java.util.Scanner;

class GCDLoop{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);

        int firstNumber, secondNumber;
        System.out.print("Firt number: ");
        firstNumber = reader.nextInt();

        System.out.print("Second number: ");
        secondNumber = reader.nextInt();

        System.out.println("The Greatest Common Divisor is: " + gcd(firstNumber, secondNumber));
    }


    public static int gcd(int number1, int number2){
        int gcdNumber = 1;
        
        for (int i = number1; i > 1; i--){
            if(number1 % i == 0 && number2 % i == 0){
                gcdNumber = i;    
                break;            
            }   
        }
        return gcdNumber;
    }
}
