public class FindMin {
    public static void main(String[] args){
        int birinciSayi = Integer.parseInt(args[0]);
        int ikinciSayi = Integer.parseInt(args[1]);
        int ucuncuSayi = Integer.parseInt(args[2]);

        int sonuc;
        int sonuc2;

        boolean kontrol = birinciSayi < ikinciSayi;
        sonuc = kontrol ? birinciSayi : ikinciSayi;
        
        boolean kontrol2 = sonuc < ucuncuSayi; 
        sonuc2 = kontrol2 ? sonuc : ucuncuSayi;

        System.out.println(sonuc2);



    }
}
