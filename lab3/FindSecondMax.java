import java.util.*;

public class FindSecondMax {
     
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Write a number how many numbers you will enter");
        int howMany = reader.nextInt();
        int number;

    	int arr[] = new int[howMany];
        for (int i = 0; i < howMany+1; i++){
            number = reader.nextInt();
            arr[i] = number;
        }
        
    	int largest = arr[0];
    	int secondLargest = arr[0];
    		
    	System.out.println("The given array is:" );
    	for (int i = 0; i < arr.length; i++) {
    		System.out.print(arr[i]+"\t");
    	}
    	for (int i = 0; i < arr.length; i++) {
     
    		if (arr[i] > largest) {
    			secondLargest = largest;
    			largest = arr[i];
     
    		} else if (arr[i] > secondLargest) {
    			secondLargest = arr[i];
     
    		}
    	}
     
    	System.out.println("\nSecond largest number is:" + secondLargest);
     
    }
}
