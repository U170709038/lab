import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); 
		Random rand = new Random();
		int number =rand.nextInt(100); 

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.println("For exit hit '-1'.");

		while (true){
            System.out.print("Can you guess it: ");
            int guess = reader.nextInt();
            if (guess == -1){
                System.out.println("See you later..");
                break;
            }
            else if (guess == number){
                System.out.println("Congratulations!");
		        reader.close();
                break;
            }
            else if (guess < number){
                System.out.println("Sorry try greater");
            }
            else if (guess > number){
                System.out.println("Sorry try less");
            }
        }
	}	
}
